//rafce
import React from 'react'

const RenderWithMap = () => {
    const userList = [
    {
        name: 'A',
        age: 16,
        address: 'HCM',
    },
    {
        name: 'B',
        age: 17,
        address: 'Ha Noi',
    },
    {
        name: 'C',
        age: 18,
        address: 'Da Nang',
    },
]

const renderUserlist = () => {
    return userList.map((user,index) => {
        return (
            <li key = {index}>
            name: {user.name}, age: {user.age}, address: {user.address}
            </li>
        )
    })
}

  return (
    <div className='container'>
        RenderWithMap
        <ul>
             {/* 1. Phải có thuộc tính key ở thẻ bao bọc ngoài cùng
                 2. Key phai là giá trị duy nhất, không bị trùng lặp
                 3. Hạn chế sử dụng index làm giá trị của thuộc tính key, chỉ nên sử dụng khi mảng tĩnh ( ko thêm xóa sửa ở danh sách cần render) */}
        {/* {
            userList.map((user, index) => {
                return (
                    <li key = {index}>
                    name: {user.name}, age: {user.age}, address: {user.address}
                    </li>
                )
            })
        } */}
        {renderUserlist()}
        </ul>
    </div>
  )
}

export default RenderWithMap