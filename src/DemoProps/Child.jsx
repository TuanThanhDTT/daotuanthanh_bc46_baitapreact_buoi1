import React from 'react'

const Child = (props) => {
    const {data, hello} = props
    console.log('data',data);
    const Hallo = hello();
    console.log(Hallo);
  return (
    <div>Child</div>
  )
}

export default Child