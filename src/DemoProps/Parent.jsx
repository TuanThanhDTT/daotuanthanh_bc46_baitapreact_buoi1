import React from 'react'
import Child from './Child';
//property
//props thay đổi => component render lại
//props => ko thể thay đổi giá trị của props ở component nhận props
//Ko thể gán lại giá trị của props ví dụ props.value = 10 nhưng có thể gán giá trị của value bằng cách value = 10, bởi vì đã sử dụng destructuring để bóc tách giá trị của props là biến mới là value rồi
const Parent = (props) => {
    console.log('props',props);
    const {value, bgColor} = props
  return (
    <div>
        <p className='display-4'>Parent {value}</p>
        <p className='display-4'>{bgColor}</p>
        <Child data ={[1, 2, 3, 4, 5]} hello = {() => {
            console.log("Hello World");
        }}/>
    </div>
  )
}

export default Parent