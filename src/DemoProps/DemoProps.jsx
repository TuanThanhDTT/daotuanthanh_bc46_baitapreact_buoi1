import React from 'react'
import Parent from './Parent'

const DemoProps = () => {
  return (
    <div className='container mt-5'>
        <h1>DemoProps</h1>
        <Parent value={1} bgColor= "red"/>
        <Parent  value={2}/>
        <Parent  value={3}/>
    </div>
  )
}

export default DemoProps