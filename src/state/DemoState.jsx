import React, { useState } from "react";
import { flushSync } from "react-dom";
const DemoState = () => {
  //state dùng để quản lí trạng thái của component
  //Những thành phần nào trên UI thay đổi khi user tương tác => cần đưa vào state
  //Để tạo 1 state trong function component sử dụng hook useState
  //useState => trả về 1 mảng gồm 2 phần tử => phần tử [0]: state, phần tử [1]: hàm để thay đổi state
  //Giá trị khởi tạo (initial value) được gán cho state khi gọi hàm useState và truyền vào tham số
  //Chỉ được thay đổi giá trị state bằng cách gọi hàm setState và truyền vào giá trị mới
  //Khi gọi hàm setState và thay đổi giá trị state => component sẽ render vói giá trị state mới => UI thay đổi
  //Trong 1 compponent có thể tạo nhiều state
  const [state, setstate] = useState(true);

  const [isShowMessage, setIsShowMessage] = useState();
  // let isLogin = true
  // const handleLogout = () => {
  //     isLogin = false
  //     console.log('isLogin', isLogin);
  // }

  //Ví dụ tăng giảm số
  //batch state update
  const [number, setNumber] = useState(0);
  const onIncreaNumber = () => {
    // setNumber(number + 1); // 0 + 1
    // setNumber(number + 1); // 0 + 1
    // setNumber(number + 1); // 0 + 1
    // setNumber(number + 1); // 0 + 1
    // setNumber(number + 1); // 0 + 1
    // setNumber((currentState) => currentState + 1 ) // 0 + 1
     // setNumber((currentState) => currentState + 1 ) // 1 + 1
     // setNumber((currentState) => currentState + 1 )  // 2 + 1
     // setNumber((currentState) => currentState + 1 )  // 3 + 1
     // setNumber((currentState) => currentState + 1 ) // 4 + 1
     flushSync(() => {
      setNumber(number + 1)
     })
     //flushSync chạy nhiều state cùng lúc
     flushSync(() => {
      setfontSize(fontSize + 2)
     })
  };
  const onDicreaNumber = () => {
    setNumber(number - 1);
  };

  //Ví dụ tăng giảm Fontsize
  const [fontSize, setfontSize] = useState(60);
  const onIncreaFontSize = () => {
    setfontSize(fontSize + 2);
  };
  const onDicreaFontSize = () => {
    setfontSize(fontSize - 2);
  };

  //Bt đổi màu ảnh xe bằng thuộc tính src của thẻ img
  const [srcImage, setSrcImage] = useState("./images/cars/black-car.jpg");
  const handleChangeCar = (nameCar) => {
    setSrcImage(`./images/cars/${nameCar}-car.jpg`);
  };

  return (
    <div className="container mt-5">
      <h1>DemoState</h1>
      <p>{state ? "Hello WonderBOSS" : "Đăng nhập"}</p>
      <button
        className="btn btn-success mt-3"
        onClick={() => {
          setstate(false);
        }}
      >
        Đăng xuất
      </button>

      <div className="mx-5 my-5">
        <p className="display-3">Number: {number}</p>
        <div className="mt-3">
          <button className="btn btn-success mr-3" onClick={onIncreaNumber}>
            increaNumber
          </button>
          <button className="btn btn-danger" onClick={onDicreaNumber}>
            decreaNumber
          </button>
        </div>
      </div>
      {/* BT Fontsize */}
      <div className="mx-5 my-5">
        <p className="display-3" style={{ fontSize: `${fontSize}px` }}>
          Hello BC 46
        </p>
        <div className="mt-3">
          <button className="btn btn-success mr-3" onClick={onIncreaFontSize}>
            increaFontSize
          </button>
          <button className="btn btn-danger" onClick={onDicreaFontSize}>
            decreaFontSize
          </button>
        </div>
      </div>

      {/* BT Chọn màu xe */}
      <div className="mt-5">
        <h2>BT Chọn xe</h2>
        <div className="row">
          <div className="col-8">
            <img className="img-fluid" src={srcImage} alt="" />
          </div>
          <div className="col-4">
            <div className="mt-5">
              <button
                className="btn btn-dark"
                onClick={() => {
                  handleChangeCar("black");
                }}
              >
                BLACK
              </button>
            </div>
            <div className="mt-5">
              <button
                className="btn btn-danger"
                onClick={() => {
                  handleChangeCar("red");
                }}
              >
                RED
              </button>
            </div>
            <div className="mt-5">
              <button
                className="btn btn-secondary"
                onClick={() => {
                  setSrcImage("./images/cars/silver-car.jpg");
                }}
              >
                SILVER
              </button>
            </div>
            <div className="mt-5">
              <button
                className="btn btn-dark"
                style={{ backgroundColor: "grey" }}
                onClick={() => {
                  setSrcImage("./images/cars/steel-car.jpg");
                }}
              >
                STEEL
              </button>
            </div>
          </div>
        </div>
      </div>
      <br />
    </div>
  );
};

export default DemoState;
