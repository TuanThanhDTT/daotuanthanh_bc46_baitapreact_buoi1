import React from 'react'

const ProductDetail = (props) => {
    const {prdDetail} = props
  return (
    <div className='row mt-3'>
        <div className="col-4">
            <img className='img-fluid' src={prdDetail.hinhAnh} alt="" />
        </div>
        <div className="col-8">
            <h2>Thông số kỹ thuật</h2>
            <table className='table'>
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Màn hình</td>
                        <td>{prdDetail.manHinh}</td>
                    </tr>
                    <tr>
                        <td>Hệ điều hành</td>
                        <td>{prdDetail.heDieuHanh}</td>
                    </tr>
                    <tr>
                        <td>Camera trước</td>
                        <td>{prdDetail.cameraTruoc}</td>
                    </tr>
                    <tr>
                        <td>Camera sau</td>
                        <td>{prdDetail.cameraSau}</td>
                    </tr>
                    <tr>
                        <td>RAM</td>
                        <td>{prdDetail.ram}</td>
                    </tr>
                    <tr>
                        <td>ROM</td>
                        <td>{prdDetail.rom}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
  )
}

export default ProductDetail