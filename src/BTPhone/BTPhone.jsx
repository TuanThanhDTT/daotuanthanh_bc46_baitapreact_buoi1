import React, { useState } from 'react'
import ProductList from './ProductList'
import data from './data.json'
import ProductDetail from './ProductDetail'
const BTPhone = () => {
    // Chi tiết sản phẩm
    const [prdDetail, setPrdDetail] = useState(data[0])

    //Danh sách giỏ hàng
    const [cart, setCart] = useState([{ "maSP": 1, "tenSP": "VinSmart Live", "manHinh": "AMOLED, 6.2, Full HD+", "heDieuHanh": "Android 9.0 (Pie)", "cameraTruoc": "20 MP", "cameraSau": "Chính 48 MP & Phụ 8 MP, 5 MP", "ram": "4 GB", "rom": "64 GB", "giaBan": 5700000, "hinhAnh": "./images/phone/vsphone.jpg" },])

    const handlePrdDetail = (products) => {
        setPrdDetail(products)
    }
  return (
    <div className='container mt-5'>
        <div className='d-flex justify-content-between'>
            <h1>BTPhone</h1>
            <button data-toggle="modal" data-target="#gioHang">Giỏ hàng</button>
        </div>
        <ProductList data = {data} handlePrdDetail = {handlePrdDetail} />

        {/* Chi tiết sản phẩm */}
        <ProductDetail prdDetail = {prdDetail}/>

        {/* Modal */}
        <div>
  <div className="modal fade" id="gioHang" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog modal-xl">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">Giỏ hàng</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">
            <table className='table'>
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên SP</th>
                        <th>Hình ảnh</th>
                        <th>Giá tiền</th>
                        <th>Số lượng</th>
                        <th>Tổng tiền</th>
                    </tr>
                </thead>
                <tbody>
                    {cart.map((cart, index) => 
                        <tr>
                            <td>{index + 1}</td>
                            <td>{cart.tenSP}</td>
                            <td><img style= {{width: 100, height: 100}}className='img-fluid' src={cart.hinhAnh} alt="" /></td>
                            <td>{cart.giaBan.toLocaleString()}</td>
                            <td>
                            <div>
                                <button className='btn btn-danger'>-
                                </button>{""}
                                {cart.soLuong}{""}
                                <button className='btn btn-success'>+
                                </button>
                            </div>
                            </td>
                        <td>{cart.soLuong * cart.giaBantoLocaleStrin()}</td>
                        </tr>)
                    }
                </tbody>
            </table>
        </div>

      </div>
      <div className="modal-footer">
      <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" className="btn btn-primary">Save changes</button>
    </div>
    </div>
  </div>
</div>
    </div>
  )
}

export default BTPhone