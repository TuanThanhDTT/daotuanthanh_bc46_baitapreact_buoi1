import React from 'react'
import ProductItem from './ProductItem';

const ProductList = (props) => {
    const { data, handlePrdDetail } = props
    console.log('data',data);
  return (
    <div className='row'>
        {
            data.map((item) => {
                return <ProductItem products = {item} key = {item.maSP} handlePrdDetail = {handlePrdDetail}/>
            })
        }
        
    </div>
  )
}

export default ProductList