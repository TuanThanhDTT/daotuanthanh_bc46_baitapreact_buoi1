import React from 'react'

const ProductItem = (props) => {
    const {products, handlePrdDetail} = props
  return (
    <div key={products.maSP} className='col-4 mt-3'>
    <div className="card">
        <img style= {{width: 350, height: 350}} src={products.hinhAnh} alt="" />
        <div className="card-body">
            <p className='font-weight-bold'>{products.tenSP}</p>
            <button className='btn btn-outline-success' onClick={() => handlePrdDetail(products)}>Xem chi tiết</button>
            <button className='btn btn-outline-info ml-3' >Thêm vào giỏ hàng</button>
        </div>
    </div>
    <br/>
    </div>
  )
}

export default ProductItem