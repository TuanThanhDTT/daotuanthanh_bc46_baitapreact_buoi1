//rafce
import React from 'react'
import movieList from './data.json'
const BTMovie = () => {
    console.log(movieList)
    return ( 
    <div className='container'>
        <h1>BTMovie</h1>
        <div className='row'>
            {
                movieList.map((movie) => {
                    return <div key = {movie.maPhim} className='col-3 mt-4'>
                                <div className="card">
                                    <img style = {{width: '250px', height: '360px'}} src={movie.hinhAnh} alt="" />
                                    <div className="card-body" style={{height: '250px', overflow: 'hidden'}}> 
                                        <p className='text-danger font-weight-bold'>{movie.tenPhim}</p>
                                        <p>{movie.moTa}</p>
                                    </div>
                                </div>
                            </div>
                })
            }
        </div>
    </div>
  )
}

export default BTMovie