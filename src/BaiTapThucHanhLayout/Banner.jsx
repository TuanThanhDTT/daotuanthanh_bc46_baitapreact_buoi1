import React from 'react'

const Banner = () => {
  return (
    <div className='Banner'>
        <div className="bg-light rounded-3 div_banner">
            <div className="m-4 m-lg-5 main_banner text-left">
                <h1 className="display-5 fw-bold banner_h1">A Warm Welcome!</h1>
                <p className="fs-4 my-3">Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
                <a className="btn btn-primary btn-lg btn_call" href="#!">Call to action</a>
            </div>
        </div>
    </div>
  )
}

export default Banner