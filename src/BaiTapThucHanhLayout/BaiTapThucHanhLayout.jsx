import React from 'react'
import Header from './Header'
import './style.scss'
import Footer from './Footer'
import Body from './Body'
const BaiTapThucHanhLayout = () => {
  return (
    <div className='BaiTapThucHanhLayout'>
        <Header/>
        <Body/>
        <Footer/>
    </div>
  )
}

export default BaiTapThucHanhLayout