import React from 'react'

const Header = () => {
  return (
    <div className="Header">
       <div id="site-header" className="main-header fixed-top nav-fixed">
	    <div >
            <nav className="navbar navbar-expand-lg px-0 pt-lg-0">
                <h1>
                    <a className="navbar-brand" href="#">
                        Start Bootstrap
                    </a>
                </h1>
                <div className="main_navbar collapse navbar-collapse" id="navbar-content">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <a href="#" className="nav-link active">Home</a>
                        </li>
                        <li className="nav-item">
                            <a  href="#" className="nav-link font-dark-header">About</a>
                        </li>
                        <li className="nav-item">
                            <a  href="#" className="nav-link font-dark-header">Services</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" className="nav-link font-dark-header">Contact</a>
                        </li>
                    </ul>
                </div>
            </nav>
	</div>
</div>
    </div>
  )
}

export default Header