import React from 'react';

const Item = () => {
  return (
    <div className='Item row'>
      <div className="card col-2">
        <img className="card-img-top" src="/images/cars/black-car.jpg" alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" className="btn btn-primary">Find Out More!</a>
        </div>
      </div>
      <div className="card col-2">
        <img className="card-img-top" src="/images/cars/red-car.jpg" alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" className="btn btn-primary">Find Out More!</a>
        </div>
      </div>
      <div className="card col-2">
        <img className="card-img-top" src="/images/cars/silver-car.jpg" alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" className="btn btn-primary">Find Out More!</a>
        </div>
      </div>
      <div className="card col-2">
        <img className="card-img-top" src="/images/cars/steel-car.jpg" alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" className="btn btn-primary">Find Out More!</a>
        </div>
      </div>
    </div>
  );
}

export default Item;
