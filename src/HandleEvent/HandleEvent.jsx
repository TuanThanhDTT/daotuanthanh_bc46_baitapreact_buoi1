import React from 'react'

const HandleEvent = () => {
    const showMessage = () => {
        alert("Message Alert")
    }
    const showMessageParam = (mess) => {
        alert(mess)
    }
  return (
    <div className='container'>
        HandleEvent
        <button className='btn btn-success ml-3' onClick={showMessage}>Show Message</button>
        <button className='btn btn-success ml-3' onClick={ () => showMessageParam("hahaha")}>Show Message Param</button>
    </div>
  )
}

export default HandleEvent