import React from 'react'

const DataBindingWithCondition = () => {
    const DataBindingWithCondition = true
    const message = 'Hello BC46'
  return (
    <div className='container mt-5'>DataBindingWithCondition
    <p>{DataBindingWithCondition ? message : 'Hello Cybersoft'}</p>
    <p>{DataBindingWithCondition && message}</p>
    </div>
  )
}

export default DataBindingWithCondition