import React from 'react'

const Content = () => {
  return (
    <div className='p-5 bg-warning display-4 font-bold text-center'>Content</div>
  )
}

export default Content