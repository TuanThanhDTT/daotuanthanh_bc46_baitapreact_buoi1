import React from 'react'
import Navigation from './Navigation'
import Footer from './Footer'
import header from './header'
import Content from './Content'
const home = () => {
  return (
    <div className="container">
        <h1 className='p-2 bg-dark text-white text-center'>Home</h1>
        <Header></Header>
        <div className='row'>
            <div className="col-4">
            <Navigation></Navigation>
            </div>
            <div className="col-8">
            <Content></Content>
            </div>
        </div>
        <Footer></Footer>
    </div>
  )
}

export default home