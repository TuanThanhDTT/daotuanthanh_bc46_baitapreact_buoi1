import React from 'react'
import Item from './Item'

const ProductList = (props) => {
    const {data, handlePrdDetail} = props
    console.log("data", data);
  return (
    <div className='row'>
        {
            data.map((item) => {
                return <Item handlePrdDetail= {handlePrdDetail}  products={item} key={item.id}/>
            })
        }  
    </div>
  )
}

export default ProductList