import React from 'react'

const Item = (props) => {
    const {products, handlePrdDetail} = props
  return (
    <div key={products.id} className='col-4 mt-3'>
    <div className="card">
        <img src={products.image} alt="" />
        <div className="card-body">
            <p className='font-weight-bold'>{products.name}</p>
            <p className='mt-3'>{products.price} $</p>
            <button className='btn btn-outline-success' data-toggle="modal" data-target="#exampleModal" onClick={() => handlePrdDetail(products)}>Xem chi tiết</button>
        </div>
    </div>
    <br/>
</div>
  )
}

export default Item